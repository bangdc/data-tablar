// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2015-11-15 using
// generator-karma 1.0.0

module.exports = function(config) {
    'use strict';

    config.set({

        reporters: ['progress', 'html', 'coverage'],

        htmlReporter: {
            outputFile: 'test/reports/units-test.html',

            // Optional
            pageTitle: 'dataTablar Unit Tests',
            subPageTitle: 'Reporters'
        },

        preprocessors: {
            'app/**/*.js': ['coverage']
        },
        coverageReporter: {
            // specify a common output directory
            dir: 'dist/reports/coverage',
            reporters: [
                // reporters not supporting the `file` property
                { type: 'html', subdir: 'report-html' },
                { type: 'text-summary', subdir: '.', file: 'text-summary.txt' },
            ]
        },

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // base path, that will be used to resolve files and exclude
        basePath: '../',

        // testing framework to use (jasmine/mocha/qunit/...)
        // as well as any additional frameworks (requirejs/chai/sinon/...)
        frameworks: [
            "jasmine"
        ],

        // list of files / patterns to load in the browser
        files: [
            "bower_components/jquery/dist/jquery.js",

            "bower_components/bootstrap/dist/js/bootstrap.js",

            "bower_components/angular/angular.js",

            'bower_components/angular-mocks/angular-mocks.js',

            "bower_components/angular-animate/angular-animate.js",
            "bower_components/angular-cookies/angular-cookies.js",
            "bower_components/angular-resource/angular-resource.js",
            "bower_components/angular-route/angular-route.js",
            "bower_components/angular-sanitize/angular-sanitize.js",
            "bower_components/angular-touch/angular-touch.js",

            "bower_components/moment/moment.js",
            "bower_components/underscore/underscore.js",
            "bower_components/jquery-ui/jquery-ui.js",
            "bower_components/angular-ui-date/src/date.js",
            "bower_components/angular-ui-notification/dist/angular-ui-notification.js",
            "bower_components/ng-dialog/js/ngDialog.js",
            "bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
            "bower_components/angular-moment/angular-moment.js",
            "bower_components/angular-ui-sortable/sortable.js",
            "bower_components/angular-translate/angular-translate.js",
            "bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js",

            "app/scripts/app.js",
            "app/scripts/controllers/employee.js",
            "app/scripts/controllers/omdb.js",
            "app/scripts/controllers/soccer.js",
            "app/scripts/controllers/product.js",
            "app/scripts/controllers/customer.js",
            "app/scripts/controllers/order.js",
            "app/scripts/controllers/stock.js",
            "app/scripts/controllers/restaurant.js",
            "app/scripts/controllers/yahoorss.js",

            "app/data-tablar/data-tablar.js",

            "app/data-tablar/scripts/directives/tbl.js",
            "app/data-tablar/scripts/directives/form.input.js",
            "app/data-tablar/scripts/directives/form.textarea.js",
            "app/data-tablar/scripts/directives/form.typeahead.js",
            "app/data-tablar/scripts/directives/form.radio.js",
            "app/data-tablar/scripts/directives/form.datepicker.js",
            "app/data-tablar/scripts/directives/form.checkbox.js",
            "app/data-tablar/scripts/directives/form.select.single.js",
            "app/data-tablar/scripts/directives/form.select.multiple.js",
            "app/data-tablar/scripts/directives/form.select.cascade.js",

            "app/data-tablar/scripts/services/validator.js",
            "app/data-tablar/scripts/services/helper.js",
            "app/data-tablar/scripts/services/exporter.js",
            "app/data-tablar/scripts/services/requests.js",

            "app/data-tablar/scripts/filters/cell-format.js",
            "app/data-tablar/scripts/filters/row-summary.js",

            "test/spec/**/*.js"
        ],

        // list of files / patterns to exclude
        exclude: [],

        // web server port
        port: 8080,

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: [
            "PhantomJS"
        ],

        // Which plugins to enable
        plugins: [
            "karma-chrome-launcher",
            "karma-phantomjs-launcher",
            "karma-jasmine",
            "karma-htmlfile-reporter",
            "karma-coverage"
        ],

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false,

        colors: true,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        // Uncomment the following lines if you are using grunt's server to run the tests
        // proxies: {
        //   '/': 'http://localhost:9000/'
        // },
        // URL root prevent conflicts with the site root
        // urlRoot: '_karma_'
    });
};
