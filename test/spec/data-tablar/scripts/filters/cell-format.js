'use strict';

describe('testing dttbCellformat filter:', function() {
    var $filter;

    beforeEach(function () {
        module('dataTablar');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should capitalize a string', function () {
        var foo = 'hello world', result;
        result = $filter('dttbCellformat')(foo, 'capitalize');
        expect(result).toEqual('HELLO WORLD');
    });
});
