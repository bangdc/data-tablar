'use strict';

describe('testing dttbRowSummary filter:', function() {
    var $filter;

    beforeEach(function () {
        module('dataTablar');

        inject(function (_$filter_) {
            $filter = _$filter_;
        });
    });

    it('should capitalize a string', function () {
        var foo = 'hello world', result;
        result = $filter('dttbRowSummary')(foo, 'capitalize');
        expect(result).toEqual('HELLO WORLD');
    });
});
