'use strict';

describe('testing exporter service:', function() {
    beforeEach(function() {
        angular.mock.module('dataTablar');
    });

    var exporter;

    beforeEach(inject(function(_exporter_) {
        exporter = _exporter_;
    }));

    it('should be defined the exporter service', function() {
        expect(exporter).toBeDefined();
    });


    /*
    exportCsv: exportCsv,
        readTable: readTable,
        checkValidFileName: checkValidFileName,
        generateCsvHeader: generateCsvHeader,
        generateCsvBody: generateCsvBody,
        downloadCsvFile: downloadCsvFile,
        generatePrintTable: generatePrintTable,
*/

    describe('testing method exporter.exportCsv:', function() {
        it('should be defined the method exporter.exportCsv', function() {
            expect(exporter.exportCsv).toBeDefined();
        });
    });

    describe('testing method exporter.readTable:', function() {
        it('should be defined the method exporter.readTable', function() {
            expect(exporter.readTable).toBeDefined();
        });
    });

    describe('testing method exporter.checkValidFileName:', function() {
        it('should be defined the method exporter.checkValidFileName', function() {
            expect(exporter.checkValidFileName).toBeDefined();
        });
    });

    describe('testing method exporter.generateCsvHeader:', function() {
        it('should be defined the method exporter.generateCsvHeader', function() {
            expect(exporter.generateCsvHeader).toBeDefined();
        });
    });

    describe('testing method exporter.generateCsvBody:', function() {
        it('should be defined the method exporter.generateCsvBody', function() {
            expect(exporter.generateCsvBody).toBeDefined();
        });
    });

    describe('testing method exporter.downloadCsvFile:', function() {
        it('should be defined the method exporter.downloadCsvFile', function() {
            expect(exporter.downloadCsvFile).toBeDefined();
        });
    });

    describe('testing method exporter.generatePrintTable:', function() {
        it('should be defined the method exporter.generatePrintTable', function() {
            expect(exporter.generatePrintTable).toBeDefined();
        });
    });
});
