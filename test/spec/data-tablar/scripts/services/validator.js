'use strict';

describe('testing validator service:', function() {


    // load the service's module
    beforeEach(module('dataTablar'));

    // instantiate service
    var validator;
    beforeEach(inject(function(_validator_) {
        validator = _validator_;
    }));


    it('should be defined the helper service', function() {
        expect(validator).toBeDefined();
    });

    describe('lengthMin', function() {
        it('test lengthMin should be FALSE', function() {
            var test = validator.validate('lengthMin', 'docongbang', 11);
            expect(test).toBe(false);
        });

        it('test lengthMin should be TRUE', function() {
            var test = validator.validate('lengthMin', 'docongbang', 5);
            expect(test).toBe(true);
        });
    });

    describe('lengthMax', function() {
        it('test lengthMax should be FALSE', function() {
            var test = validator.validate('lengthMax', 'docongbang', 5);
            expect(test).toBe(false);
        });

        it('test lengthMax should be TRUE', function() {
            var test = validator.validate('lengthMax', 'docongbang', 11);
            expect(test).toBe(true);
        });
    });

    describe('required', function() {
        it('test required should be FALSE', function() {
            var test = validator.validate('required', null, true);
            expect(test).toBe(false);
        });

        it('test required should be TRUE', function() {
            var test = validator.validate('required', [1, 2, 3], true);
            expect(test).toBe(true);
        });
    });

    // TODO
    describe('requiredStrictly', function() {
        it('test requiredStrictly should be FALSE', function() {
            var test = validator.validate('requiredStrictly', null, true);
            expect(true).toBe(false);
        });
    });

    describe('type', function() {
        it('test type STRING should be TRUE', function() {
            var test = validator.validate('type', 'docongbang', 'string');
            expect(test).toBe(true);
        });

        it('test type NUMBER INT should be TRUE', function() {
            var test = validator.validate('type', 10, 'number');
            expect(test).toBe(true);
        });

        it('test type NUMBER FLOAT should be TRUE', function() {
            var test = validator.validate('type', 10.1512, 'number');
            expect(test).toBe(true);
        });

        it('test type ARRAY should be TRUE', function() {
            var test = validator.validate('type', [1, 2, 3, '14124'], 'array');
            expect(test).toBe(true);
        });

        it('test type OBJECT should be TRUE', function() {
            var test = validator.validate('type', { a: 'a', b: 'b' }, 'object');
            expect(test).toBe(true);
        });

        it('test type EMAIL should be TRUE', function() {
            var test = validator.validate('type', 'google.test@gmail.com', 'email');
            expect(test).toBe(true);
        });
    });

    describe('containing', function() {
        it('test containing with STRING should be TRUE', function() {
            var test = validator.validate('containing', 'docongbang', 'do');
            expect(test).toBe(true);
        });

        it('test containing with NUMBER should be TRUE', function() {
            var test = validator.validate('containing', 123124.54, 31);
            expect(test).toBe(true);
        });

        it('test containing with ARRAY should be TRUE', function() {
            var test = validator.validate('containing', [1, 2, 3, 'a', 'b'], 'a');
            expect(test).toBe(true);
        });
    });

    describe('notContaining', function() {
        it('test notContaining with STRING should be TRUE', function() {
            var test = validator.validate('notContaining', 'docongbang', 'dox');
            expect(test).toBe(true);
        });

        it('test notContaining with NUMBER should be TRUE', function() {
            var test = validator.validate('notContaining', 123124.54, '2xa');
            expect(test).toBe(true);
        });

        it('test notContaining with ARRAY should be TRUE', function() {
            var test = validator.validate('notContaining', [1, 2, 3, 'a', 'b'], 'x');
            expect(test).toBe(true);
        });
    });

    // TODO
    describe('regex', function() {
        it('test regex should be FALSE', function() {
            var test = validator.validate('regex', null, true);
            expect(test).toBe(false);
        });
    });

    // TODO
    describe('checkWithServer', function() {
        it('test checkWithServer should be FALSE', function() {
            var test = validator.validate('checkWithServer', null, true);
            expect(test).toBe(false);
        });
    });

    describe('Comparasion', function() {
        it('test greaterThan should be FALSE', function() {
            var test = validator.validate('greaterThan', 2, 5);
            expect(test).toBe(false);
        });

        it('test equalTo should be FALSE', function() {
            var test = validator.validate('equalTo', 2, 3);
            expect(test).toBe(false);
        });

        it('test lessThan should be FALSE', function() {
            var test = validator.validate('lessThan', 5, 2);
            expect(test).toBe(false);
        });

        describe('lessEqual', function() {
            it('test lessEqual should be TRUE', function() {
                var test = validator.validate('lessEqual', 2, 2);
                expect(test).toBe(true);
            });

            it('test lessEqual should be TRUE', function() {
                var test = validator.validate('lessEqual', 2, 3);
                expect(test).toBe(true);
            });
        });

        describe('greaterEqual', function() {
            it('test greaterEqual should be FALSE', function() {
                var test = validator.validate('greaterEqual', 5, 5);
                expect(test).toBe(true);
            });

            it('test greaterEqual should be FALSE', function() {
                var test = validator.validate('greaterEqual', 5.12, 3);
                expect(test).toBe(true);
            });
        });

        describe('Different', function() {
            it('test not should be FALSE', function() {
                var test = validator.validate('not', 'a', 'a');
                expect(test).toBe(false);
            });

            it('test not should be TRUE', function() {
                var test = validator.validate('not', 'a', 'b');
                expect(test).toBe(true);
            });

            it('test not should be TRUE', function() {
                var test = validator.validate('not', 21, 312);
                expect(test).toBe(true);
            });
        });
    });


    describe('inArray', function() {
        it('test inArray should be FALSE', function() {
            var test = validator.validate('inArray', 'a', ['ab', ['c']]);
            expect(test).toBe(false);
        });
    });

    describe('optionsMin', function() {
        it('test optionsMin should be FALSE', function() {
            var test = validator.validate('optionsMin', null, true);
            expect(test).toBe(false);
        });
    });

    describe('optionsMax', function() {
        it('test optionsMax should be FALSE', function() {
            var test = validator.validate('optionsMax', null, true);
            expect(test).toBe(false);
        });
    });

    describe('date', function() {
        it('test dateFrom should be FALSE', function() {
            var test = validator.validate('dateFrom', null, true);
            expect(test).toBe(false);
        });

        it('test dateUntil should be FALSE', function() {
            var test = validator.validate('dateUntil', null, true);
            expect(test).toBe(false);
        });

        it('test dateBetween should be FALSE', function() {
            var test = validator.validate('dateBetween', null, true);
            expect(test).toBe(false);
        });
    });

    describe('time', function() {
        it('test timeFrom should be FALSE', function() {
            var test = validator.validate('timeFrom', null, true);
            expect(test).toBe(false);
        });

        it('test timeUntil should be FALSE', function() {
            var test = validator.validate('timeUntil', null, true);
            expect(test).toBe(false);
        });

        it('test timeBetween should be FALSE', function() {
            var test = validator.validate('timeBetween', null, true);
            expect(test).toBe(false);
        });
    });

});
