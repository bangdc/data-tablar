'use strict';

describe('testing translator service:', function() {
	beforeEach(function() {
        angular.mock.module('dataTablar');
    });

    var translator;

    beforeEach(inject(function(_translator_) {
        translator = _translator_;
    }));

    it('should be defined the translator service', function() {
        expect(translator).toBeDefined();
    });
});