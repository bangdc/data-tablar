'use strict';

describe('testing requests service:', function() {
    beforeEach(function() {
        angular.mock.module('dataTablar');
    });

    var requests;

    beforeEach(inject(function(_requests_) {
        requests = _requests_;
    }));

    it('should be defined the requests service', function() {
        expect(requests).toBeDefined();
    });
});
