'use strict';

describe('testing helper service:', function() {
    //beroe each test it, loading the module dataTablar.

    beforeEach(function() {
        angular.mock.module('dataTablar');
    });
    //to test the SERVICE helper, we need to inject it. 
    var helper;
    // as we pass the _helper_, it means the injector will look for 'helper' in module dataTablar. 
    // The _x_ is just a naming convention here.
    beforeEach(inject(function(_helper_) {
        helper = _helper_;
    }));

    it('should be defined the helper service', function() {
        expect(helper).toBeDefined();
    });


    //it's now time to do test

    describe('testing method helper.flattenArrayOfNestedObjects:', function() {
        it('should be defined the method helper.flattenArrayOfNestedObjects', function() {
            expect(helper.flattenArrayOfNestedObjects).toBeDefined();
        });
    });

    describe('testing method helper.convertOutReadingRequest:', function() {
        it('should be defined the method helper.convertOutReadingRequest', function() {
            expect(helper.convertOutReadingRequest).toBeDefined();
        });
    });

    describe('testing method helper.convertRequestedResult:', function() {
        it('should be defined the method helper.convertRequestedResult', function() {
            expect(helper.convertRequestedResult).toBeDefined();
        });
    });

    describe('testing method helper.readTable:', function() {
        it('should be defined the method helper.readTable', function() {
            expect(helper.readTable).toBeDefined();
        });
    });

    describe('testing method helper.checkValidFileName:', function() {
        it('should be defined the method helper.checkValidFileName', function() {
            expect(helper.checkValidFileName).toBeDefined();
        });
    });

    describe('testing method helper.generatePagination:', function() {
        it('should be defined the method helper.generatePagination', function() {
            expect(helper.generatePagination).toBeDefined();
        });
    });

    describe('testing method helper.generatePrintTable:', function() {
        it('should be defined the method helper.generatePrintTable', function() {
            expect(helper.generatePrintTable).toBeDefined();
        });
    });

    describe('testing method helper.generateCsvText:', function() {
        it('should be defined the method helper.generateCsvText', function() {
            expect(helper.generateCsvText).toBeDefined();
        });
    });

    describe('testing method helper.generateCsvHeader:', function() {
        it('should be defined the method helper.generateCsvHeader', function() {
            expect(helper.generateCsvHeader).toBeDefined();
        });
    });

    describe('testing method helper.downloadCsvFile:', function() {
        it('should be defined the method helper.downloadCsvFile', function() {
            expect(helper.downloadCsvFile).toBeDefined();
        });
    });

    describe('testing method helper.checkHttpMethod:', function() {
        it('should be defined the method helper.checkHttpMethod', function() {
            expect(helper.checkHttpMethod).toBeDefined();
        });
    });

    describe('testing method helper.replaceParamsUrl:', function() {
        it('should be defined the method helper.replaceParamsUrl', function() {
            expect(helper.replaceParamsUrl).toBeDefined();
        });
    });

    describe('testing method helper.compare:', function() {
        it('should be defined the method helper.compare', function() {
            expect(helper.compare).toBeDefined();
        });
    });

    describe('Testing method helper.sum', function() {
        it('should be defined the method helper.sum', function() {
            expect(helper.sum).toBeDefined();
        });
        var $elements = [{
            'a': 1,
            'b': 2
        }, {
            'a': 3,
            'b': 4
        }];
        it('should return 0', function() {
            expect(helper.sum($elements, 'x')).toEqual(0);
        });

        it('should return 4', function() {
            expect(helper.sum($elements, 'a')).toEqual(4);
        });
    });
});
