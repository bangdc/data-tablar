'use strict';

/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormCheckbox
 * @param {scope} model Value of form
 * @param {string} label Label of form
 * @param {object} validations Validation configuration object
 * @param {object} config Other configurations object
 * @param {boolean} valid State of validation
 * @description
 * # dttbFormCheckbox
 * Form element as checkbox is used while cell updating, row creating, row updating etc.
 * 
 * @scope
 * @restrict 'E'
 */
angular.module('dataTablar')
    .directive('dttbFormCheckbox', ['DTTB_BASE_FOLDER', dttbFormCheckbox]);

function dttbFormCheckbox(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-checkbox.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            config: '=',
            valid: '=?',
        },
        restrict: 'E',
        controller: dttbFormCheckboxController
    };

    return directive;
}
dttbFormCheckboxController.$inject = ['$scope', 'dttbValidatorService'];

function dttbFormCheckboxController($scope, dttbValidatorService) {
    $scope.modelChecking = [];

    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];

    $scope.$watchCollection('model', function(newVal) {
        angular.forEach($scope.config.options.data, function(opt, key) {
            $scope.modelChecking[key] = (_.indexOf(newVal, opt[$scope.config.options.modelKey]) > -1);
        });
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });

    $scope.verifyCheckedValue = function(value) {
        return (_.indexOf($scope.model, value) > -1) ? true : false;
    };

    $scope.checkingValue = function(value) {
        if (!$scope.model) {
            $scope.model = [];
        }
        var index = _.indexOf($scope.model, value);
        if ($scope.model.length > 0 && index > -1) {
            $scope.model = _.without($scope.model, $scope.model[index]);
        } else {
            $scope.model.push(value);
        }
    };
}