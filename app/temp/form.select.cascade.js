'use strict';

/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormSelectcascade
 * @param {scope} model Value of form
 * @param {string} label Label of form
 * @param {object} validations Validation configuration object
 * @param {object} config Other configurations object
 * @param {boolean} valid State of validation
 * @param {boolean} pristine Determine whether user has changed the form's value.
 * @description
 * # dttbFormSelectcascade
 * Form element as cascade select is used while cell updating, row creating, row updating etc.
 * 
 * @scope
 * @restrict 'E'
 */
angular.module('dataTablar')
  .directive('dttbFormSelectcascade', ['DTTB_BASE_FOLDER', dttbFormSelectcascade]);

function dttbFormSelectcascade(DTTB_BASE_FOLDER) {
  var directive = {
    priority: 0,
    templateUrl: DTTB_BASE_FOLDER + '/views/form-select-cascade.html',
    scope: {
      model: '=',
      label: '@?',
      validations: '=?',
      config: '=',
      valid: '=?',
      pristine: '=?',
    },
    restrict: 'E',
    controller: dttbFormSelectcascadeController
  };
  return directive;
};
dttbFormSelectcascadeController.$inject = ['$scope', 'dttbValidatorService', '$http', 'Notification'];

function dttbFormSelectcascadeController($scope, dttbValidatorService, $http, Notification) {
  var $lastIndex = $scope.config.options.cascade.length - 1;
  var $depth = $scope.config.options.cascade.length;
  angular.forEach($scope.config.options.cascade, function(select, index) {
    console.warn(index);
    $scope.$watch(function() {
      return select.value;
    }, function(newValue, oldValue, $scope) {
      if (newValue !== oldValue) {
        if (index !== $lastIndex) {
          var i = index + 1;
          while (i < $depth) {
            $scope.config.options.cascade[i].data = [];
            i++;
          }
          var $requestConfig = {};
          $http.get($scope.config.options.cascade[index + 1].requestConfig.url, $requestConfig)
            .then(function successCallback(response) {
              $scope.config.options.cascade[index + 1].data = response.data.data;
              Notification.success('All select options were loaded.');
            }, function errorCallback(response) {
              Notification.error(response.status + ' ' + response.statusText);
            });
        } else {
          $scope.model = newValue;
        }
      }
    }, true);
  });
};