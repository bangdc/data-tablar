'use strict';

/**
 * @ngdoc function
 * @name dataTablarApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dataTablarApp
 */
angular.module('dataTablarApp')
	.controller('MainCtrl', function($scope) {
		$scope.configDataTablar = {
			optionAjaxRequests: [],
			table: {
				uniqueId: 'envato-items',
				displaySelectRowsPage: false,
				displayPaging: true,
				displayShowingRow: true,
				label: 'ENVATO Items',
				primaryLabel: '',
				displayNavHighlights: true,
				displayNavArrange: true,
				displayNavLocalSort: true,
				displayNavHideShow: true,
				displayNavLanguage: true,
				displayNavExport: true,
				displayNavPrint: true,
				displayNavRefresh: true,
				displayNavShare: true,
				primaryKey: 'id',
				numRowsPerPage: 10,
				numPagesDisplaying: 5,
			},
			columns: [{
				dataType: 'number',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: false,
				label: 'ID',
				tooltip: 'Item id',
				key: 'id',
				displayKey: 'id',
				displayAs: 'number',
				displayAsConfig: {},
				formTypeConfig: {},
			}, {
				dataType: 'text',
				formType: '',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Name',
				tooltip: 'Item Name',
				key: 'url',
				displayKey: 'name',
				displayAs: 'url',
				displayAsConfig: {
					textCase: 'capitalizeEach',
				},
				formTypeConfig: {},
				highlights: [{
					type: 'text-primary h4 dttb-text-bold',
					condition: 'containing',
					value: 'admin',
				}, {
					type: 'text-success h4 dttb-text-bold',
					condition: 'containing',
					value: 'datatablar',
				}, {
					type: 'text-info h4 dttb-text-bold',
					condition: 'containing',
					value: 'angular',
				}, ],
			}, {
				dataType: 'boolean',
				formType: '',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Trending',
				tooltip: 'Trending',
				key: 'trending',
				displayKey: 'trending',
				displayAs: 'boolean',
				displayAsConfig: {
					booleanAs: 'icon',
					booleanFalseVal: 'fa fa-volume-off',
					booleanTrueVal: 'fa fa-volume-up',
				},
				formTypeConfig: {},
				highlights: [{
					type: 'bg-primary',
					condition: '===',
					value: true,
				}, {
					type: 'bg-warning',
					condition: '===',
					value: false,
				}, ],
			}, {
				dataType: 'number',
				formType: '',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Sales',
				tooltip: 'Number of Sales',
				key: 'number_of_sales',
				displayKey: 'number_of_sales',
				displayAs: 'number',
				displayAsConfig: {},
				formTypeConfig: {},
				highlights: [{
					type: 'text-primary h4 dttb-text-bold',
					condition: '>=',
					value: '100',
				}, {
					type: 'text-info h4 dttb-text-bold',
					condition: '<',
					value: '100',
				}, ],
			}, {
				dataType: 'number',
				formType: '',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Price',
				tooltip: 'Item Price',
				key: 'price',
				displayKey: 'price',
				displayAs: 'currency',
				displayAsConfig: {
					currency: '$',
				},
				formTypeConfig: {},
				highlights: [{
					type: 'bg-success',
					condition: '<=',
					value: '10',
				}, {
					type: 'bg-info',
					condition: '<=',
					value: '20',
				}, {
					type: 'bg-primary',
					condition: '<=',
					value: '30',
				}, ],
			}, {
				dataType: 'number',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: false,
				label: 'Rating Point',
				tooltip: 'Rating point',
				key: 'rating_point',
				displayKey: 'rating_point',
				displayAs: 'number',
				displayAsConfig: {},
				formTypeConfig: {},
				highlights: [{
					type: 'bg-primary',
					condition: '>=',
					value: '4',
				}, {
					type: 'bg-success',
					condition: '<',
					value: '4',
				}, {
					type: 'bg-success',
					condition: '>=',
					value: '3',
				}, {
					type: 'bg-info',
					condition: '<',
					value: '3',
				}, {
					type: 'bg-info',
					condition: '>=',
					value: '2',
				}, {
					type: 'bg-warning',
					condition: '<',
					value: '2',
				}, ],
			}, {
				dataType: 'number',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: false,
				label: 'Rating Count',
				tooltip: 'Rating count',
				key: 'rating_count',
				displayKey: 'rating_count',
				displayAs: 'number',
				displayAsConfig: {},
				formTypeConfig: {},
			}, {
				dataType: 'text',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Author',
				tooltip: 'Author',
				key: 'author_url',
				displayKey: 'author_username',
				displayAs: 'url',
				displayAsConfig: {},
				formTypeConfig: {},
			}, {
				dataType: 'text',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Description',
				tooltip: 'Item Description',
				key: 'description',
				displayKey: 'description',
				displayAs: 'text',
				displayAsConfig: {},
				formTypeConfig: {},
			}, {
				dataType: 'text',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: false,
				label: 'Thumbnail',
				tooltip: 'Preview Image',
				key: 'thumbnail_preview',
				displayKey: 'thumbnail_preview',
				displayAs: 'image',
				displayAsConfig: {},
				formTypeConfig: {},
			}, {
				dataType: 'array',
				formType: '',
				headerDirection: 'horizontal',
				sortable: false,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Tags',
				tooltip: 'Tags',
				key: 'tags',
				displayKey: 'tags',
				displayAs: 'array',
				displayAsConfig: {
					arrayAs: 'text',
				},
				formTypeConfig: {},
			}, {
				dataType: 'text',
				formType: '',
				headerDirection: 'horizontal',
				sortable: true,
				uneditable: true,
				RTLtext: false,
				allowLocalFilter: true,
				label: 'Classification',
				tooltip: 'Item classification',
				key: 'classification_url',
				displayKey: 'classification',
				displayAs: 'url',
				displayAsConfig: {},
				formTypeConfig: {},
			}, ],
			actions: {
				reading: {
					requestConfig: {
						method: 'GET',
						url: 'http://localhost:5003/',
					},
					filters: [{
						label: 'Search',
						placeholder: 'Keyword',
						key: 'term',
					}, {
						label: 'Site',
						placeholder: 'Envato market site',
						key: 'site',
						options: [
							'themeforest.net',
							'codecanyon.net',
							'photodune.net',
							'videohive.net',
							'graphicriver.net',
							'audiojungle.net',
							'3docean.net',
						],
					}, {
						label: 'Last Updated',
						placeholder: 'Last updated time',
						key: 'date',
						options: [
							'this-year',
							'this-month',
							'this-week',
							'this-day',
						],
					}, {
						label: 'Price min',
						placeholder: 'Minimum price.',
						key: 'price_min',
					}, {
						label: 'Price max',
						placeholder: 'Maximum price',
						key: 'price_max',
					}, {
						label: 'Minimum rating',
						placeholder: 'Minimum rating',
						key: 'rating_min',
					}, ],
					conversionResponse: function(response) {
						// @param {object} response Response object from request. 

						// @returns {object} The final response 
						// Start your conversion function of response from the line below.
						var formattedResponse = {
							dttbRows: response.data.items,
							dttbNumTotalRows: response.data.total_items
						};
						return {
							dttbResponse: formattedResponse,
							dttbSuccess: true
						};

						// End your conversion function before this line.

					},
				},
			},
		};
	});