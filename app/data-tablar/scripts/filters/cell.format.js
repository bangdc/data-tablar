'use strict';
/**
 * @ngdoc filter
 * @name dataTablar.filter:dttbCellFormat
 *
 * @requires $filter
 * @requires moment
 * @requires DTTB_DATE_DISPLAY_FORMAT
 * 
 * @param {string|boolean|number|date|array|object} input Value input
 * @param {string} displayAs Type of input's value. E.g. text, array, number, date and boolean.
 * @param {object} displayAsConfig displayAsConfiguration object
 * 
 * @function
 * @description
 * # dttbCellFormat
 * This filter is used to customize diplays of value on table. E.g. number, text, array, date etc.
 */
angular.module('dataTablar')
    .filter('dttbCellFormat', ['$filter', 'moment', 'DTTB_DATE_DISPLAY_FORMAT', function($filter, moment, DTTB_DATE_DISPLAY_FORMAT) {
        return function(input, displayAs, displayAsConfig) {
            if (!angular.isObject(displayAsConfig)) {
                displayAsConfig = {};
            }
            var result;
            switch (displayAs) {
                case 'text':
                    result = input || '';
                    if (displayAsConfig.textCase === 'uppercase') {
                        result = result.toUpperCase();
                    }
                    if (displayAsConfig.textCase === 'lowercase') {
                        result = result.toLowerCase();
                    }
                    if (displayAsConfig.textCase === 'capitalizeEach') {
                        try {
                            result = result.replace(/(?:^|\s)\S/g, function(a) {
                                return a.toUpperCase();
                            });
                        } catch (e) {
                            console.warn(e);
                        }
                    }
                    result = $filter('limitTo')(result, parseInt(displayAsConfig.limitToLength), parseInt(displayAsConfig.limitToBegin));
                    break;
                case 'ordinal':
                    result = parseInt(input);
                    var lastDigit = result % 10;
                    var postfix;
                    if (lastDigit === 1) {
                        postfix = 'st';
                    } else if (lastDigit === 2) {
                        postfix = 'nd';
                    } else if (lastDigit === 3) {
                        postfix = 'rd';
                    } else {
                        postfix = 'th';
                    }
                    result += postfix;
                    break;
                case 'number':
                    result = $filter('number')(Number(input), parseInt(displayAsConfig.fractionSize) || 0);
                    break;
                case 'currency':
                    result = $filter('currency')(Number(input), displayAsConfig.currency, parseInt(displayAsConfig.fractionSize) || 0);
                    break;
                case 'array':
                    if (displayAsConfig.arrayAs === 'list') {
                        result = '<ul>';
                        angular.forEach(input, function(value) {
                            result += '<li>';
                            result += '<span>';
                            result += value;
                            result += '</span>';
                            result += '</li>';
                        });
                        result += '</ul>';
                    } else if (displayAsConfig.arrayAs === 'text') {
                        try {
                            result = input.join('; ');
                        } catch (e) {
                            result = input;
                            console.warn(e);
                        }
                    }
                    break;
                case 'date':
                    var dateFormat = displayAsConfig.dateFormat || DTTB_DATE_DISPLAY_FORMAT;
                    try {
                        result = moment(input).format(dateFormat);
                    } catch (e) {
                        console.warn(e);
                        result = input;
                    }
                    break;
                case 'boolean':
                    if (displayAsConfig.booleanAs === 'icon') {
                        if (input) {
                            result = '<span class="text-success h4"><i class="' + displayAsConfig.booleanTrueVal + '"></i></span>';
                        } else {
                            result = '<span class="text-default h4"><i class="' + displayAsConfig.booleanFalseVal + '"></i></span>';
                        }
                    } else if (displayAsConfig.booleanAs === 'text') {
                        if (input) {
                            result = displayAsConfig.booleanTrueVal;
                        } else {
                            result = displayAsConfig.booleanFalseVal;
                        }
                    } else {
                        result = input;
                    }
                    break;
                default:
                    result = input;
            }
            return result;
        };
    }]);