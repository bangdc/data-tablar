'use strict';
/**
 * @ngdoc filter
 * @name dataTablar.filter:dttbRowSummary
 *
 * @requires $filter
 * @requires dttbHelperService
 * 
 * @param {string} columnKey Column to be summarized
 * @param {array} rows Array of objects. All rows's data in table.
 * @param {number|string} type Type of summary. 0: sum; 1: average; 2: min; 3: max;
 * 
 * @function
 * @description
 * # dttbRowSummary
 * This filter is used to summarize values of a column. 
 */
angular.module('dataTablar')
    .filter('dttbRowSummary', ['$filter', 'dttbHelperService', function($filter, dttbHelperService) {
        return function(columnKey, rows, type) {
            var result;
            if (!rows || !rows.length) {
                return result;
            }
            switch (type) {
                case 0:
                    result = dttbHelperService.sum(rows, columnKey);
                    break;
                case 1:
                    result = dttbHelperService.sum(rows, columnKey) / rows.length;
                    break;
                case 2:
                    result = _.min(rows, function(row) {
                        return row[columnKey];
                    })[columnKey];
                    break;
                case 3:
                    result = _.max(rows, function(row) {
                        return row[columnKey];
                    })[columnKey];
                    break;
                case 'mean':
                    break;
                default:
                    result = '-';
                    break;
            }
            return result;
        };
    }]);