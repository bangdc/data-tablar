'use strict';

/**
 * @ngdoc overview
 * @name dataTablar
 * @description
 * #dataTablar
 * Main module of the application.
 */
angular
    .module('dataTablar', [
        'ui.bootstrap',
        'ui-notification',
        'angularMoment',
        'ngDialog',
        'pascalprecht.translate',
        'ngSanitize',
        'LocalStorageModule',
        'ng-sortable'
    ])
    .constant('DTTB_EVENTS', {
        requestFailed: 'DTTB_REQUEST_FAILED'
    })
    .constant('DTTB_HELP_EMAIL', 'help@datatablar.com')
    .constant('DTTB_BASE_FOLDER', 'data-tablar')
    .constant('DTTB_DATE_DISPLAY_FORMAT', 'YYYY-MM-DD')
    .constant('DTTB_EXPORT_DELIMITER', ',')
    .constant('DTTB_DICTIONARY', DTTB_DICTIONARY)
    .config(['localStorageServiceProvider', function(localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('dataTablar');
        localStorageServiceProvider
            .setStorageType('sessionStorage');
    }])
    .config([
        'NotificationProvider',
        '$translateProvider',
        function(NotificationProvider, $translateProvider) {
            NotificationProvider.setOptions({
                delay: 5000,
                startTop: 20,
                startRight: 10,
                verticalSpacing: 20,
                horizontalSpacing: 20,
                positionX: 'left',
                positionY: 'bottom'
            });
            $translateProvider.useStaticFilesLoader({
                prefix: '/data-tablar/scripts/dictionaries/', // path to translations files
                suffix: '.json' // suffix, currently- extension of the translations
            });
            $translateProvider.preferredLanguage('en_US'); // is applied on first load
            $translateProvider.useSanitizeValueStrategy('sanitize');
        }
    ])
    .run([
        '$rootScope',
        'DTTB_BASE_FOLDER',
        'DTTB_HELP_EMAIL',
        'DTTB_DICTIONARY',
        function(
            $rootScope,
            DTTB_BASE_FOLDER,
            DTTB_HELP_EMAIL,
            DTTB_DICTIONARY
        ) {
            $rootScope.dataTablarDictionary = DTTB_DICTIONARY;
            $rootScope.dataTablarBaseFolder = DTTB_BASE_FOLDER;
            $rootScope.dataTablarHelpEmail = DTTB_HELP_EMAIL;
        }
    ]);