'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormTextarea
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * @param {boolean} disabled. Binding - optional - '=?'. Enable / Disable state.
 * 
 * @description
 * # dttbFormTextarea
 * Form element as textarea for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormTextarea', ['DTTB_BASE_FOLDER', dttbFormTextarea]);

function dttbFormTextarea(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-textarea.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            valid: '=?',
            config: '=',
            disabled: '=?'
        },
        controller: dttbFormTextareaController,
        restrict: 'E'
    };
    return directive;
}

dttbFormTextareaController.$inject = ['$scope', 'dttbValidatorService'];

function dttbFormTextareaController($scope, dttbValidatorService) {
    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];

    $scope.$watch('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}