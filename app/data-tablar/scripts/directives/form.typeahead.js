'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormTypeahead
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * @param {boolean} disabled. Binding - optional - '=?'. Enable / Disable state.
 * 
 * @description
 * # dttbFormTypeahead
 * Form element as typeahead for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormTypeahead', ['DTTB_BASE_FOLDER', dttbFormTypeahead]);

function dttbFormTypeahead(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-typeahead.html',
        scope: {
            model: '=',
            label: '@?',
            disabled: '=?',
            validations: '=?',
            config: '=',
            valid: '=?',
        },
        restrict: 'E',
        controller: dttbFormTypeaheadController,
    };

    return directive;
}

dttbFormTypeaheadController.$inject = [
    '$scope',
    'dttbValidatorService',
    '$rootScope',
    'dttbRequestService',
    'Notification',
    'dttbHelperService'
];

function dttbFormTypeaheadController(
    $scope,
    dttbValidatorService,
    $rootScope,
    dttbRequestService,
    Notification,
    dttbHelperService
) {
    if ($scope.config.hasOwnProperty('typeaheadAjaxRequestIndex')) {
        $scope.config.typeaheadAjax = $rootScope.dttbOptionAjaxRequests[parseInt($scope.config.typeaheadAjaxRequestIndex)];
        $scope.fetchOptions = function(val) {
            try {
                var typeaheadAjax = $scope.config.typeaheadAjax;

                var httpConfig = angular.copy(typeaheadAjax.requestConfig);
                dttbRequestService.initHttpObject(httpConfig);

                var conversionRequest = angular.copy(typeaheadAjax.conversionRequest);
                var conversionResponse = angular.copy(typeaheadAjax.conversionResponse);

                httpConfig.params['typeahead_value'] = val;

                return dttbRequestService.callRequest(httpConfig, conversionRequest, conversionResponse)
                    .then(function(response) {
                        return response.dttbOptions.map(function(item) {
                            if (!$scope.config.typeaheadAjax.typeaheadKey || !$scope.config.typeaheadAjax.typeaheadKey.length) {
                                return item;
                            } else {
                                return item[$scope.config.typeaheadAjax.typeaheadKey];
                            }
                        });
                    });
            } catch (e) {
                console.warn(e, $rootScope.dataTablarDictionary.formTypeAheadAjaxError);
                Notification.error({
                    message: dttbHelperService.generateSolvingErrorGuide($rootScope.dataTablarDictionary.formTypeAheadAjaxError, e),
                    title: $rootScope.dataTablarDictionary.notifErrorTitle
                });
            }
        };
    }


    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];
    $scope.$watch('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}