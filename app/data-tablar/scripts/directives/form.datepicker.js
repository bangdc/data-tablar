'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormDatepicker
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * @param {boolean} disabled. Binding - optional - '=?'. Enable / Disable state.
 * 
 * @description
 * # dttbFormDatepicker
 * Form element as date-picker for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormDatepicker', ['DTTB_BASE_FOLDER', dttbFormDatepicker]);

function dttbFormDatepicker(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-date-picker.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            config: '=?',
            valid: '=?',
            disabled: '=?'
        },
        restrict: 'E',
        controller: dttbFormDatepickerController
    };
    return directive;
}

dttbFormDatepickerController.$inject = ['$scope', 'dttbValidatorService', 'moment', 'DTTB_DATE_DISPLAY_FORMAT'];

function dttbFormDatepickerController($scope, dttbValidatorService, moment, DTTB_DATE_DISPLAY_FORMAT) {
    if ($scope.model && $scope.model.toString().length > 0) {
        $scope.datePickerModel = moment($scope.model).toDate();
    } else {
        $scope.datePickerModel = moment().toDate();
    }
    $scope.$watch('datePickerModel', function(newValue) {
        $scope.model = moment(newValue).format(DTTB_DATE_DISPLAY_FORMAT);

        $scope.valid = true;

        angular.forEach($scope.validations, function(validation, key) {
            if (key === 'dateSince' || key === 'dateUntil') {
                validation.valid = dttbValidatorService.validate(key, moment($scope.model, DTTB_DATE_DISPLAY_FORMAT), moment(validation.rule, DTTB_DATE_DISPLAY_FORMAT));
            } else {
                validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            }
            $scope.valid = $scope.valid && validation.valid;
        });
    });
    $scope.popupOpened = false;
    $scope.openPopup = function() {
        $scope.popupOpened = true;
    };
}