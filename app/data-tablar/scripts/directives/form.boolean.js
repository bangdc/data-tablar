'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormBoolean
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * 
 * @description
 * # dttbFormBoolean
 * Form element as boolean for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormBoolean', ['DTTB_BASE_FOLDER', dttbFormBoolean]);

function dttbFormBoolean(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-boolean.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            config: '=',
            valid: '=?',
        },
        restrict: 'E',
        controller: dttbFormBooleanController
    };
    return directive;
}

dttbFormBooleanController.$inject = ['$scope', 'dttbValidatorService'];

function dttbFormBooleanController($scope, dttbValidatorService) {
    $scope.modelChecking = [];

    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];

    $scope.$watchCollection('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}