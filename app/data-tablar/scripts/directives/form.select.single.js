'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormSelectsingle
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * @param {boolean} disabled. Binding - optional - '=?'. Enable / Disable state.
 * 
 * @description
 * # dttbFormSelectsingle
 * Form element as select-single for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormSelectsingle', ['DTTB_BASE_FOLDER', dttbFormSelectsingle]);

function dttbFormSelectsingle(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-select-single.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            config: '=',
            valid: '=?',
            disabled: '=?'
        },
        restrict: 'E',
        controller: dttbFormSelectsingleController
    };
    return directive;
}

dttbFormSelectsingleController.$inject = ['$scope', 'dttbValidatorService', 'dttbRequestService'];

function dttbFormSelectsingleController($scope, dttbValidatorService, dttbRequestService) {
    dttbRequestService.handleOptionAjaxRequest($scope.config);

    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];
    $scope.$watch('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}