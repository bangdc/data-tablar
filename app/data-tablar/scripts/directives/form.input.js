'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormInput
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {string} type. Attribute - optional '@?'. Type of this form element. (E.g column.dataType)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - optional - '=?'. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * @param {boolean} disabled. Binding - optional - '=?'. Enable / Disable state.
 * 
 * @description
 * # dttbFormInput
 * Form element as input for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormInput', ['DTTB_BASE_FOLDER', dttbFormInput]);

function dttbFormInput(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-input.html',
        scope: {
            model: '=',
            label: '@?',
            type: '@?',
            validations: '=?',
            valid: '=?',
            config: '=?',
            disabled: '=?'
        },
        controller: dttbFormInputController,
        restrict: 'E'
    };

    return directive;
}

dttbFormInputController.$inject = ['$scope', 'dttbValidatorService'];

function dttbFormInputController($scope, dttbValidatorService) {
    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];
    $scope.$watch('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}