'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormSelectmultiple
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * @param {boolean} disabled. Binding - optional - '=?'. Enable / Disable state.
 * 
 * @description
 * # dttbFormSelectmultiple
 * Form element as select-multiple for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormSelectmultiple', ['DTTB_BASE_FOLDER', dttbFormSelectmultiple]);

function dttbFormSelectmultiple(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-select-multiple.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            config: '=',
            valid: '=?',
            disabled: '=?'
        },
        restrict: 'E',
        controller: dttbFormSelectmultipleController
    };
    return directive;
}

dttbFormSelectmultipleController.$inject = ['$scope', 'dttbValidatorService', 'dttbRequestService'];

function dttbFormSelectmultipleController($scope, dttbValidatorService, dttbRequestService) {
    dttbRequestService.handleOptionAjaxRequest($scope.config);

    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];
    $scope.$watch('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}