'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbConvertToNumber
 * 
 * @description
 * Convert ng-model from string to number. For example in select form element.
 */
angular.module('dataTablar')
    .directive('dttbConvertToNumber', [dttbConvertToNumber]);

function dttbConvertToNumber() {
    var directive = {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function(val) {
                return '' + val;
            });
        }
    };
    return directive;
}