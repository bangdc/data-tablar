'use strict';
/**
 * @ngdoc directive
 * @name dataTablar.directive:dttbFormRadio
 * @restrict 'E'
 * @scope
 * 
 * @param {model} model. Binding - required - '='. Value of this form element.
 * @param {string} label. Attribute - optional '@?'. Label of this form element. (E.g column.label)
 * @param {object} validations. Binding - optional - '=?'. Configuration object of validations for this form element.
 * @param {object} config. Binding - required - '='. Configuration object for this form element. (E.g column.formTypeConfig)
 * @param {boolean} valid. Binding - optional - '=?'. State of validation.
 * 
 * @description
 * # dttbFormRadio
 * Form element as radio for data-tablar table.
 */

angular.module('dataTablar')
    .directive('dttbFormRadio', ['DTTB_BASE_FOLDER', dttbFormRadio]);

function dttbFormRadio(DTTB_BASE_FOLDER) {
    var directive = {
        priority: 0,
        templateUrl: DTTB_BASE_FOLDER + '/views/form-radio.html',
        scope: {
            model: '=',
            label: '@?',
            validations: '=?',
            config: '=',
            valid: '=?',
        },
        restrict: 'E',
        controller: dttbFormRadioController
    };
    return directive;
}

dttbFormRadioController.$inject = ['$scope', 'dttbValidatorService', 'dttbRequestService'];

function dttbFormRadioController($scope, dttbValidatorService, dttbRequestService) {
    dttbRequestService.handleOptionAjaxRequest($scope.config);

    $scope.valid = !_.keys($scope.validations).length;
    $scope.valids = [];
    $scope.$watch('model', function() {
        $scope.valid = true;
        angular.forEach($scope.validations, function(validation, key) {
            validation.valid = dttbValidatorService.validate(key, $scope.model, validation.rule);
            $scope.valid = $scope.valid && validation.valid;
        });
    });
}