'use strict';
/**
 * @ngdoc service
 * @name dataTablar.service:dttbExporterService
 * 
 * @requires $filter
 * @requires DTTB_EXPORT_DELIMITER
 * 
 * @description
 * # dttbExporterService
 * It provides exportCsv service to generate table's data to CSV format, 
 * and generatePrintTable service to generate table print-able format.
 */
angular.module('dataTablar')
    .factory('dttbExporterService', ['$filter', 'DTTB_EXPORT_DELIMITER', function($filter, DTTB_EXPORT_DELIMITER) {
        return {
            exportCsv: exportCsv,
            generatePrintTable: generatePrintTable,
        };

        /**
         * @ngdoc method
         * @name dataTablar.service: dttbExporterService # generatePrintTable
         * @methodOf dataTablar.service:dttbExporterService
         * 
         * @description
         * 
         * @param {object} printingConfig Printing options object
         * @param {object} rows All rows data to be printed
         * @param {object} columns All columns to be printed
         * 
         * @returns {string} The HTML generated page includes table codes. 
         **/
        function generatePrintTable(printingConfig, rows, columns) {
            try {
                var result = '';
                result += '<table style="width:100%" border="1">';
                result += '<tbody>';
                //header
                result += '<tr>';
                if (printingConfig.header) {
                    angular.forEach(columns, function(col) {
                        if (printingConfig.selectedCols.indexOf(col.key) > -1) {
                            result += '<th>';
                            result += col.label;
                            result += '</th>';
                        }
                    });
                }
                result += '</tr>';
                //body
                angular.forEach(rows, function(row, rowIndex) {
                    if (printingConfig.selectedRows.indexOf(rowIndex) > -1) {
                        result += '<tr>';
                        angular.forEach(columns, function(col) {
                            if (printingConfig.selectedCols.indexOf(col.key) > -1) {
                                result += '<td>';
                                var val = row[col.key];
                                switch (col.displayAs) {
                                    case 'text':
                                        if (val.toString().length === 0) {
                                            val = "";
                                        } else {
                                            if (!angular.isObject(col.displayAsConfig)) {
                                                col.displayAsConfig = {};
                                            }
                                            col.displayAsConfig.limitToBegin = 0;
                                            col.displayAsConfig.limitToLength = val.length;
                                            val = $filter('dttbCellFormat')(val, 'text', col.displayAsConfig);
                                        }
                                        break;
                                    case 'boolean':
                                        if (col.displayAsConfig.booleanAs === 'icon') {
                                            val = val ? 'TRUE' : 'FALSE';
                                        } else {
                                            val = $filter('dttbCellFormat')(val, 'boolean', col.displayAsConfig);
                                        }
                                        break;
                                    case 'currency':
                                    case 'number':
                                    case 'ordinal':
                                    case 'date':
                                        val = $filter('dttbCellFormat')(val, col.displayAs, col.displayAsConfig);
                                        break;
                                    case 'image':
                                        val = '<img src="' + val + '">';
                                        break;
                                    default:
                                        break;
                                }
                                result += val;
                                result += '</td>';
                            }
                        });
                        result += '</tr>';
                    }
                });
                //footer
                if (printingConfig.footer) {
                    angular.forEach(columns, function(col) {
                        if (printingConfig.selectedCols.indexOf(col.key) > -1) {
                            result += '<th>';
                            result += col.label;
                            result += '</th>';
                        }
                    });
                }
                result += '</tbody>';
                result += '</table>';

                return result;
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbExporterService#exportCsv
         * @methodOf dataTablar.service:dttbExporterService
         * 
         * @param {object} printingConfig Printing options object
         * @param {object} columns All columns to be printed
         * @param {object} rows All rows data to be printed
         * 
         * @description 
         * Export table's data to csv file and download.
         **/
        function exportCsv(exportCsvConfig, columns, rows) {
            var fileName = checkValidFileName(exportCsvConfig.fileName),
                csvBody = '',
                csvHeader = '',
                csvFooter = '';
            var selectedRowsData = [];
            angular.forEach(exportCsvConfig.selectedRows, function(rowIndex) {
                selectedRowsData.push(rows[rowIndex]);
            });
            try {
                if (exportCsvConfig.header) {
                    csvHeader = generateCsvHeader(columns, exportCsvConfig);
                }
                if (exportCsvConfig.footer) {
                    csvFooter = generateCsvHeader(columns, exportCsvConfig);
                }
                csvBody = generateCsvBody(columns, selectedRowsData, exportCsvConfig);
                downloadCsvFile(csvHeader, csvBody, csvFooter, fileName);
                return true;
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbExporterService#checkValidFileName
         * @methodOf dataTablar.service:dttbExporterService
         * 
         * @param {string} name The filename to be checked.
         *
         * @description 
         * Check whether the filename is valid or not.
         *
         * @return {string} Valid filename or 'data-tablar' if invalid.
         **/
        function checkValidFileName(name) {
            if (!/^[A-Za-z0-9]+/.test(name)) {
                name = 'data-tablar';
            }
            return name;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbExporterService#generateCsvHeader
         * @methodOf dataTablar.service:dttbExporterService
         * 
         * @param {object} columns The scope object of columns to be generated.
         * @param {object} exportConfig Export configuration object.
         *
         * @description 
         * Generate table's headers/footers data in format CSV.
         * 
         * @returns {string} The table's headers/footers in format CSV.
         **/
        function generateCsvHeader(columns, exportConfig) {
            var csvHeader = '';

            angular.forEach(columns, function(col) {
                if (exportConfig.selectedCols.indexOf(col.key) > -1) {
                    csvHeader += '"' + col.label + '"' + DTTB_EXPORT_DELIMITER;
                }
            });
            csvHeader = csvHeader.substring(0, csvHeader.lastIndexOf(DTTB_EXPORT_DELIMITER)) + '\r\n';

            return csvHeader;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbExporterService#generateCsvBody
         * @methodOf dataTablar.service:dttbExporterService
         * 
         * @param {object} columns The scope object of columns to be generated.
         * @param {object} rows The scope object of rows to be generated.
         * @param {object} exportConfig Export configuration object.
         *
         * @description 
         * Generate table's body data in format CSV.
         *
         * @returns {string} The table's body in format CSV.
         **/
        function generateCsvBody(columns, rows, exportConfig) {
            var csvBody = '';
            angular.forEach(rows, function(row) {
                var newRowCsv = '';
                angular.forEach(columns, function(col) {
                    if (exportConfig.selectedCols.indexOf(col.key) > -1) {
                        var val = row[col.displayKey];
                        switch (col.displayAs) {
                            case 'text':
                                if (val.toString().length === 0) {
                                    val = "";
                                } else {
                                    if (!angular.isObject(col.displayAsConfig)) {
                                        col.displayAsConfig = {};
                                    }
                                    col.displayAsConfig.limitToBegin = 0;
                                    col.displayAsConfig.limitToLength = val.length;
                                    val = $filter('dttbCellFormat')(val, 'text', col.displayAsConfig);
                                    if (angular.isString(val)) {
                                        val = val.replace(/"/g, '""'); // Escape double quotes
                                    }
                                }
                                break;
                            case 'boolean':
                                if (col.displayAsConfig.booleanAs === 'icon') {
                                    val = val ? 'TRUE' : 'FALSE';
                                } else {
                                    val = $filter('dttbCellFormat')(val, 'boolean', col.displayAsConfig);
                                }
                                break;
                            case 'array':
                                col.displayAsConfig.arrayAs = 'text';
                                val = $filter('dttbCellFormat')(val, 'array', col.displayAsConfig);
                                break;
                            case 'currency':
                            case 'number':
                            case 'ordinal':
                            case 'date':
                                val = $filter('dttbCellFormat')(val, col.displayAs, col.displayAsConfig);
                                break;
                            default:
                                break;
                        }
                        newRowCsv += '"' + val + '"' + DTTB_EXPORT_DELIMITER;
                    }
                });
                csvBody += newRowCsv.substring(0, newRowCsv.lastIndexOf(DTTB_EXPORT_DELIMITER)) + '\r\n';
            });
            return csvBody;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbExporterService#downloadCsvFile
         * @methodOf dataTablar.service:dttbExporterService
         * 
         * @param {string} csvHeader Table's headers
         * @param {string} csvBody Table's body
         * @param {string} csvFooter Table's footers
         * @param {string} fileName The CSV filename.
         *
         * @description 
         * Let user to download their table's data as CSV file format. 
         * The download is automatically triggered.
         **/
        function downloadCsvFile(csvHeader, csvBody, csvFooter, fileName) {
            var csvText = csvHeader + csvBody + csvFooter;
            var downloadLink = 'data:application/octet-stream;base64,' + btoa(unescape(encodeURIComponent(csvText)));
            var link = angular.element('<a href="' + downloadLink + '" download="' + fileName + '.csv' + '" target="_blank"></a>');
            angular.element(document.body).append(link);
            link[0].click();
            link.remove();
            return;
        }
    }]);