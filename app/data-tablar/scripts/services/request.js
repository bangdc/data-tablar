'use strict';
/**
 * @ngdoc service
 * @name dataTablar.service:dttbRequestService
 * 
 * @requires $http
 * @requires Notification
 * 
 * @description
 * # dttbRequestService
 * Provided services for communicating with remote server by HTTP requests.
 */
angular.module('dataTablar')
    .factory('dttbRequestService', [
        '$http',
        '$q',
        'Notification',
        '$rootScope',
        'dttbHelperService',
        'DTTB_EVENTS',
        function($http, $q, Notification, $rootScope, dttbHelperService, DTTB_EVENTS) {
            return {
                callRequest: callRequest,
                initHttpObject: initHttpObject,
                handleRouteParams: handleRouteParams,
                handleOptionAjaxRequest: handleOptionAjaxRequest
            };

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbRequestService # initHttpObject
             * @methodOf dataTablar.service:dttbRequestService
             * 
             * @description
             * Initialize params; data; headers for http config object
             * 
             * @param {object} httpConfig Reference of the http config object.
             **/
            function initHttpObject(httpConfig) {
                if (angular.isObject(httpConfig)) {
                    if (!httpConfig.hasOwnProperty('params')) {
                        httpConfig.params = {};
                    }
                    if (!httpConfig.hasOwnProperty('data')) {
                        httpConfig.data = {};
                    }
                    if (!httpConfig.hasOwnProperty('headers')) {
                        httpConfig.headers = {};
                    }
                }
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbRequestService # handleRouteParams
             * @methodOf dataTablar.service:dttbRequestService
             * 
             * @description
             * 
             * @param {string} url Url resource
             * @param {object} row Data row.
             * 
             * @returns {string} The formatted url
             **/
            function handleRouteParams(url, row) {
                angular.forEach(row, function(value, key) {
                    if (typeof value === 'string' || typeof value === 'number') {
                        url = url.replace(':' + key, value);
                    }
                });
                return url;
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbRequestService # handleOptionAjaxRequest
             * @methodOf dataTablar.service:dttbRequestService
             * 
             * @description
             * Bind the window to this event in order to prevent
             * accidentally leaving the page while
             * doing action like: UDPATE, DELETE, CREATE.
             **/
            function handleOptionAjaxRequest(formConfig) {
                if (formConfig.hasOwnProperty('optionAjaxRequestIndex')) {
                    var optionAjaxRequest = $rootScope.dttbOptionAjaxRequests[parseInt(formConfig.optionAjaxRequestIndex)];
                    if (optionAjaxRequest) {
                        if (optionAjaxRequest.callAtEachEvent) {
                            var callMinimumInterval = 0;
                            if (optionAjaxRequest.hasOwnProperty('callMinimumInterval')) {
                                callMinimumInterval = parseInt(optionAjaxRequest.callMinimumInterval);
                            }

                            var readyToCall = true;
                            var currentTime = new Date().getTime();
                            if (callMinimumInterval > 0) {
                                if ((currentTime - optionAjaxRequest.callLastTime) < callMinimumInterval) {
                                    readyToCall = false;
                                }
                            }
                            if (readyToCall) {
                                try {
                                    var httpConfig = angular.copy(optionAjaxRequest.requestConfig);
                                    initHttpObject(httpConfig);

                                    var conversionRequest = angular.copy(optionAjaxRequest.conversionRequest);
                                    var conversionResponse = angular.copy(optionAjaxRequest.conversionResponse);
                                    callRequest(httpConfig, conversionRequest, conversionResponse)
                                        .then(function(response) {
                                            optionAjaxRequest.options = response.dttbOptions;
                                            optionAjaxRequest.callLastTime = new Date().getTime();
                                            formConfig.options = optionAjaxRequest.options;
                                        });
                                } catch (e) {
                                    console.warn(e, $rootScope.dataTablarDictionary.formOptionsAjaxError);
                                    Notification.error({
                                        message: dttbHelperService.generateSolvingErrorGuide($rootScope.dataTablarDictionary.formOptionsAjaxError, e),
                                        title: $rootScope.dataTablarDictionary.notifErrorTitle
                                    });
                                }
                            }
                        }
                        formConfig.options = optionAjaxRequest.options;
                    }
                }
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbRequestService # callRequest
             * @methodOf dataTablar.service:dttbRequestService
             * 
             * @description
             * 
             * @param {object} requestConfig HTTP request configuration.
             * @param {function} conversionRequest Function to be called before requesting
             * @param {function} conversionResponse Function to be called after requesting
             * 
             * @returns {string} The HTML generated page includes table codes. 
             **/
            function callRequest(requestConfig, conversionRequest, conversionResponse) {
                //Check valid method and url
                //handle conversion request
                var finalRequestConfig = requestConfig;
                if (angular.isFunction(conversionRequest)) {
                    try {
                        finalRequestConfig = conversionRequest(requestConfig);
                    } catch (e) {
                        finalRequestConfig = requestConfig;
                        console.warn(e, $rootScope.dataTablarDictionary.notifErrorConversionRequest);
                    }
                } else {
                    finalRequestConfig = requestConfig;
                }
                //if valid, then do request and return promise. also handle conversion response
                return $http(finalRequestConfig)
                    .then(function(response) {
                        var result = response.data;
                        //Call the conversionResponse if have any.
                        if (angular.isFunction(conversionResponse)) {
                            try {
                                result = conversionResponse(response);
                            } catch (e) {
                                Notification.error({
                                    message: dttbHelperService.generateSolvingErrorGuide($rootScope.dataTablarDictionary.notifErrorConversionResponse, e),
                                    title: $rootScope.dataTablarDictionary.notifErrorTitle
                                });
                                console.warn(e, $rootScope.dataTablarDictionary.notifErrorConversionResponse);
                                return $q.reject(response);
                            }
                        }
                        //Handle the formatting response.
                        if (angular.isObject(result) &&
                            result.hasOwnProperty('dttbSuccess') &&
                            result.hasOwnProperty('dttbResponse')) {
                            if (result.dttbSuccess) {
                                return result.dttbResponse;
                            } else {
                                if (angular.isString(result.dttbResponse)) {
                                    Notification.error({
                                        message: result.dttbResponse,
                                        title: $rootScope.dataTablarDictionary.notifErrorTitle
                                    });
                                } else {
                                    Notification.error({
                                        message: dttbHelperService.generateSolvingErrorGuide($rootScope.dataTablarDictionary.notifErrorConversionResponse, response.config.url),
                                        title: $rootScope.dataTablarDictionary.notifErrorTitle
                                    });
                                }
                                return $q.reject(response);
                            }
                        } else {
                            Notification.error({
                                message: dttbHelperService.generateSolvingErrorGuide($rootScope.dataTablarDictionary.notifErrorRequestResultFormat, response.config.url),
                                title: $rootScope.dataTablarDictionary.notifErrorTitle
                            });
                            return $q.reject(response);
                        }
                    }, function(response) {
                        var title = '';
                        title += response.config.method;
                        title += ' ';
                        title += response.status;
                        title += ' ';
                        title += response.statusText;
                        //Notify error message
                        Notification.error({
                            message: response.config.url,
                            title: title
                        });
                        //Broadcast an event
                        $rootScope.$broadcast(DTTB_EVENTS.requestFailed, {
                            response: response
                        });
                        //Reject
                        return $q.reject(response);
                    });
            }
        }
    ]);