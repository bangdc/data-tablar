'use strict';
/**
 * @ngdoc service
 * @name dataTablar.service:dttbHelperService
 *
 * @requires $window
 * 
 * @description
 * # dttbHelperService
 * Provided useful functions.
 */
angular.module('dataTablar')
    .factory('dttbHelperService', [
        '$window',
        '$rootScope',
        function($window, $rootScope) {
            return {
                flattenArrayOfNestedObjects: flattenArrayOfNestedObjects,
                checkValidFileName: checkValidFileName,
                sum: sum,
                generatePagination: generatePagination,
                compare: compare,
                bindBeforeUnload: bindBeforeUnload,
                unBindBeforeUnload: unBindBeforeUnload,

                generateSolvingErrorGuide: generateSolvingErrorGuide
            };

            // <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # generateSolvingErrorGuide
             * @methodOf dataTablar.service:dttbHelperService
             *
             * @param {string} errorMsg Error message
             * @param {string} subErrorMsg For example: an url.
             * 
             * @description
             * Generate process guide to solve the error.
             **/
            function generateSolvingErrorGuide(errorMsg, subErrorMsg) {
                var processGuide = '';
                processGuide += '<blockquote>';
                processGuide += '<p><b>';
                processGuide += errorMsg;
                processGuide += '</b></p>';
                processGuide += '<footer>';
                processGuide += subErrorMsg || '';
                processGuide += '</footer>';
                processGuide += '</blockquote>';

                return processGuide;
            }
            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # bindBeforeUnload
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * Bind the window to this event in order to prevent
             * accidentally leaving the page while
             * doing action like: UDPATE, DELETE, CREATE.
             **/
            function bindBeforeUnload() {
                $window.onbeforeunload = function(e) {
                    return true;
                };
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # unBindBeforeUnload
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * Unbind the window from that event if user cancels / finishes his/her action.
             * E.g Create, Update, Delete 
             **/
            function unBindBeforeUnload() {
                $window.onbeforeunload = null;
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # compare
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * 
             * @param {any} value Value to compare
             * @param {string} condition Compare condition
             * @param {any} reference Reference value to compared with.
             * 
             * @returns {boolean} True false.
             **/
            function compare(value, condition, reference) {
                var result;
                switch (condition) {
                    case '>':
                        result = (Number(value) > Number(reference));
                        break;
                    case '<':
                        result = (Number(value) < Number(reference));
                        break;
                    case '>=':
                        result = (Number(value) > Number(reference) || Number(value) === Number(reference));
                        break;
                    case '<=':
                        result = (Number(value) < Number(reference) || Number(value) === Number(reference));
                        break;
                    case '===':
                        result = (value === reference);
                        break;
                    case '!==':
                        result = (value !== reference);
                        break;
                    case 'minLength':
                        result = (value.toString().length < reference) ? false : true;
                        break;
                    case 'maxLength':
                        result = (value.toString().length > reference) ? false : true;
                        break;
                    case 'containing':
                        result = (value.toString().toLowerCase().indexOf(reference.toLowerCase()) > -1) ? true : false;
                        break;
                    default:
                        result = false;
                        break;
                }
                return result;
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # generatePagination
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * 
             * @param {number} numTotalRows Current total rows on table
             * @param {number} numRowsPerPage Current number of rows per page
             * @param {number} numPagesDisplaying Current displayed pages
             * @param {number} currentPage Current page
             * @param {number} currentNumRows Current number of rows
             * 
             * @returns {object} The pagination object.
             **/
            function generatePagination(numTotalRows, numRowsPerPage, numPagesDisplaying, currentPage, currentNumRows) {
                var result = {
                    pagination: {},
                };
                result.pagination.totalPages = Math.ceil(numTotalRows / numRowsPerPage);
                var startDisplayingPage = Math.floor(currentPage / numPagesDisplaying) * numPagesDisplaying + 1;
                if ((currentPage % numPagesDisplaying) === 0) {
                    startDisplayingPage = currentPage - numPagesDisplaying + 1;
                }
                if ((result.pagination.totalPages - startDisplayingPage) < numPagesDisplaying) {
                    result.pagination.pages = _.range(startDisplayingPage, result.pagination.totalPages + 1);
                    result.pagination.next = 0;
                    result.pagination.last = 0;
                } else {
                    result.pagination.next = startDisplayingPage + numPagesDisplaying;
                    result.pagination.pages = _.range(startDisplayingPage, result.pagination.next);
                    result.pagination.last = result.pagination.totalPages;
                }
                if (result.pagination.pages[0] > numPagesDisplaying) {
                    result.pagination.prev = result.pagination.pages[0] - 1;
                    result.pagination.first = 1;
                } else {
                    result.pagination.prev = 0;
                    result.pagination.first = 0;
                }
                if (currentNumRows > 0) {
                    result.rowFrom = (currentPage - 1) * numRowsPerPage + 1;
                    result.rowUntil = result.rowFrom + currentNumRows - 1;
                } else {
                    result.rowFrom = 0;
                    result.rowUntil = 0;
                }
                return result;
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # flattenRecursive
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * Recursive function support for flattenArrayOfNestedObjects
             * 
             * @param {object} currentKey currentKey
             * @param {object} into into
             * @param {object} target target
             **/
            function flattenRecursive(currentKey, into, target) {
                for (var i in into) {
                    if (into.hasOwnProperty(i)) {
                        var newKey = i;
                        var newVal = into[i];

                        if (currentKey.length > 0) {
                            newKey = currentKey + '.' + i;
                        }

                        if (_.isObject(newVal) && !_.isArray(newVal) /*ypeof newVal === "object"*/ ) {
                            flattenRecursive(newKey, newVal, target);
                        } else {
                            target[newKey] = newVal;
                        }
                    }
                }
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # flattenArrayOfNestedObjects
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * 
             * @param {array} arrayOfNestedObjects Make it only one root object.
             * 
             * @returns {array} The flatted objects array.
             **/
            function flattenArrayOfNestedObjects(arrayOfNestedObjects) {
                var arrayOfFlatObject = [];
                angular.forEach(arrayOfNestedObjects, function(nestedObject, key) {
                    arrayOfFlatObject[key] = {};
                    flattenRecursive("", nestedObject, arrayOfFlatObject[key]);
                });
                return arrayOfFlatObject;
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # checkValidFileName
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * Check wether the file name is valid.
             * 
             * @param {string} name Filename
             * 
             * @returns {boolean} Valid or not. True or False.
             **/
            function checkValidFileName(name) {
                return /^[A-Za-z0-9]+/.test(name);
            }

            /**
             * @ngdoc method
             * @name dataTablar.service: dttbHelperService # sum
             * @methodOf dataTablar.service:dttbHelperService
             * 
             * @description
             * 
             * @param {object} elements array of object to be summed.
             * @param {object} key Property of object to be calculated.
             * 
             * @returns {number} The sum value. 
             **/
            function sum(elements, key) {
                if (angular.isUndefined(elements) && angular.isUndefined(key)) {
                    return 0;
                }
                var sum = 0;
                angular.forEach(elements, function(v) {
                    sum = sum + v[key];
                });
                return 0 + sum;
            }
        }
    ]);