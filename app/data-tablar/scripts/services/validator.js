'use strict';
/**
 * @ngdoc service
 * @name dataTablar.service:dttbValidatorService
 * 
 * @requires moment
 * 
 * @description
 * # dttbValidatorService
 * Provided services for form validations.
 */
angular.module('dataTablar')
    .factory('dttbValidatorService', ['moment', function(moment) {
        return {
            validate: function(rule, value, reference) {
                var result;
                switch (rule) {

                    //
                    case 'minLength':
                        result = checkLengthMin(value, reference);
                        break;

                        //
                    case 'maxLength':
                        result = checkLengthMax(value, reference);
                        break;

                        //
                    case 'required':
                        result = checkRequired(value, reference);
                        break;

                        //
                    case 'requiredStrictly':
                        //result = checkRequiredStrictly(value, reference);
                        break;

                        //
                    case 'type':
                        result = checkType(value, reference);
                        break;

                        //
                    case 'containing':
                        result = checkContaining(value, reference);
                        break;

                    case 'notContaining':
                        result = !checkContaining(value, reference);
                        break;

                    case 'regex':
                        result = checkRegex();
                        break;

                    case 'greaterThan':
                        result = checkGreaterThan(value, reference);
                        break;

                    case 'lessThan':
                        result = checkLessThan(value, reference);
                        break;

                    case 'equalTo':
                        result = checkEqualTo(value, reference);
                        break;

                    case 'greaterEqual':
                        result = checkGreaterEqual(value, reference);
                        break;

                    case 'lessEqual':
                        result = checkLessEqual(value, reference);
                        break;

                    case 'not':
                        result = checkNot(value, reference);
                        break;

                    case 'inArray':
                        result = checkInArray(value, reference);
                        break;

                    case 'optionsMin':
                        result = checkOptionsMin(value, reference);
                        break;

                    case 'optionsMax':
                        result = checkOptionsMax(value, reference);
                        break;

                    case 'dateSince':
                        result = checkDateSince(value, reference);
                        break;

                    case 'dateUntil':
                        result = checkDateUntil(value, reference);
                        break;

                    default:
                        result = true;
                        break;
                }
                return result;
            }
        };
        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#getVarType
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * getVarType(obj) returns string
         * based on the value passed in obj
         *
         * @param {any} obj Value to be checked type
         * 
         * @returns {string} type of obj
         */
        function getVarType(obj) {
            return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkRequired
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * checkRequired() returns boolean
         * based on the passed in tag name
         *
         * @param {value} value The value to be checked
         * @param {boolean} required Validation condition. True - It is required.
         * 
         * @returns {boolean} true/false - valid/invalid
         */

        function checkRequired(value, required) {
            try {
                if (required) {
                    // TODO: Find a more elegant solution to check required.
                    return (value.toString().length > 0);
                }
                return true;
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkRequiredStrictly
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Validate required strictly.
         *
         * @param {any} value Number to be checked
         * @param {any} reference Number to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */

        function checkRequiredStrictly(value, reference) {
            var valid = false;
            angular.forEach(reference.data, function(ref) {
                if (ref[reference.modelKey] === value) {
                    valid = true;
                }
            });
            return valid;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkType
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check special types. E.g. email, url, etc.
         *
         * @param {number|string} value Value to be checked
         * @param {string} reference Type to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkType(value, type) {
            var varType = getVarType(value);
            var result;
            switch (type) {
                case 'email':
                    if (varType === 'string') {
                        return /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/.test(value);
                    }
                    result = false;
                    break;
                default:
                    result = (varType === type.toLowerCase());
                    break;
            }
            return result;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkContaining
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check whether a string/number/array contains a specific value.
         *
         * @param {string|number|array} value String/number/array to check in.
         * @param {string|number|boolean} reference Value to be checked.
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkContaining(value, reference) {
            var type = getVarType(value);
            if (type === 'string' || type === 'number') {
                return (value.toString().indexOf(reference.toString()) > -1);
            }
            if (type === 'array') {
                return _.contains(value, reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#generatePrintTable
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * check regex
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkRegex() {
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#generatePrintTable
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Compare 2 number
         *
         * @param {number} value Number to be checked
         * @param {number} reference Number to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkGreaterThan(value, reference) {
            if (getVarType(value) === 'number' && getVarType(reference) === 'number') {
                return (value > reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkEqualTo
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Compare 2 number
         *
         * @param {number} value Number to be checked
         * @param {number} reference Number to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkEqualTo(value, reference) {
            if (getVarType(value) === 'number' && getVarType(reference) === 'number') {
                return (value === reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkLessThan
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Compare 2 number
         *
         * @param {number} value Number to be checked
         * @param {number} reference Number to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkLessThan(value, reference) {
            if (getVarType(value) === 'number' && getVarType(reference) === 'number') {
                return (value < reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkGreaterEqual
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Compare 2 number
         *
         * @param {number} value Number to be checked
         * @param {number} reference Number to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkGreaterEqual(value, reference) {
            if (getVarType(value) === 'number' && getVarType(reference) === 'number') {
                return (value > reference || value === reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkLessEqual
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Compare 2 number
         *
         * @param {number} value Number to be checked
         * @param {number} reference Number to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkLessEqual(value, reference) {
            if (getVarType(value) === 'number' && getVarType(reference) === 'number') {
                return (value < reference || value === reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkNot
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check inequality
         *
         * @param {value} value Value to be checked
         * @param {value} reference Value to check with.
         *
         * @returns {Boolean} true/false - valid/invalid
         */

        // TODO comparasion based on value types: string, number, array, object, etc.
        function checkNot(value, reference) {
            if (getVarType(value) === getVarType(reference)) {
                return (value !== reference);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkInArray
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check if a value is in array
         *
         * @param {value} value Value to be checked
         * @param {array} reference Array to check with
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkInArray(value, reference) {
            if (getVarType(reference) === 'array') {
                return _.contains(reference, value);
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkOptionsMin
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check minimum length of array.
         *
         * @param {array} value Array to be checked
         * @param {number} reference Minimum length of array.
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkOptionsMin(value, reference) {
            if (getVarType(value) === 'array') {
                return (!(value.length < parseInt(reference)));
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkOptionsMax
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check maximum length of array.
         *
         * @param {array} value Array to be checked
         * @param {number} reference Maximum length of array.
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkOptionsMax(value, reference) {
            if (getVarType(value) === 'array') {
                return !(value.length > parseInt(reference));
            }
            return false;
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkDateSince
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check if date is after a date
         *
         * @param {date} value Date to be checked
         * @param {date} reference Milestone to check with.
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkDateSince(value, reference) {
            try {
                return value.isAfter(reference.subtract(1, 'd'));
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkDateUntil
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check if date is before a date
         *
         * @param {date} value Date to be checked
         * @param {date} reference Milestone to check with.
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkDateUntil(value, reference) {
            try {
                return value.isBefore(reference.add(1, 'd'));
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkLengthMin
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check minimum length of string
         *
         * @param {string} value String text to be checked.
         * @param {number} reference The minimum length, e.g., 5, 15, etc.
         * 
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkLengthMin(value, reference) {
            try {
                return !(value.length < parseInt(reference));
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkLengthMax
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * Check maximum length of string
         *
         * @param {string} value String text to be checked.
         * @param {number} reference The maximum length, e.g., 5, 15, etc.
         * 
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkLengthMax(value, reference) {
            try {
                return !(value.length > parseInt(reference));
            } catch (e) {
                console.warn(e);
                return false;
            }
        }

        /**
         * @ngdoc method
         * @name dataTablar.service:dttbValidatorService#checkWithServer
         * @methodOf dataTablar.service:dttbValidatorService
         * @description
         * checkWithServer() returns boolean value
         * based on the passed in tag name
         *
         * @returns {Boolean} true/false - valid/invalid
         */
        function checkWithServer() {
            return false;
        }
    }]);