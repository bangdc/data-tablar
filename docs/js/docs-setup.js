NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "dataTablar",
      "shortName": "dataTablar",
      "type": "overview",
      "moduleName": "dataTablar",
      "shortDescription": "#dataTablar",
      "keywords": "api application datatablar main module overview"
    },
    {
      "section": "api",
      "id": "dataTablar.controller:dttbTableController",
      "shortName": "dttbTableController",
      "type": "controller",
      "moduleName": "dataTablar",
      "shortDescription": "dttbTableController",
      "keywords": "$filter $location $rootscope $route $routeparams $scope $translate ajax api appear automatically button call callatinit called cell changes check checktableconfig clicking column conditions configuration configure controller csv current data datatablar ddttbtablecontroller defined details dialog directive download dttb dttbexporterservice dttbhelperservice dttbrequestservice dttbtable dttbtablecontroller event export exporttable fetch fetched file filter generate generated generatepagination generates hide hideloading importfromshareurl includes init initialize initoptionajaxrequests keyword locally localstorageservice located main manage method moment named navigation ngdialog notification object option options pagination parameters performed performs position processed prompted properly properties property query readtable refresh refreshtable remote rendered representing request requests responsible row scope script scroll scrolltoelement server setallcolumnstitledirection shared showing showloading sort spinner stored submit submitcellupdating submitfiltering table tablequery tablestate time triggered triggerlocalsorting true url user valid variable viewmodel wether"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbConvertToNumber",
      "shortName": "dttbConvertToNumber",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "Convert ng-model from string to number. For example in select form element.",
      "keywords": "api convert datatablar directive element example form ng-model number select string"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormBoolean",
      "shortName": "dttbFormBoolean",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormBoolean",
      "keywords": "api attribute binding boolean column config configuration data-tablar datatablar directive dttbformboolean element form formtypeconfig label model object optional required table valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormDatepicker",
      "shortName": "dttbFormDatepicker",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormDatepicker",
      "keywords": "api attribute binding column config configuration data-tablar datatablar date-picker directive disable disabled dttbformdatepicker element enable form formtypeconfig label model object optional required table valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormInput",
      "shortName": "dttbFormInput",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormInput",
      "keywords": "api attribute binding column config configuration data-tablar datatablar datatype directive disable disabled dttbforminput element enable form formtypeconfig input label model object optional required table type valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormRadio",
      "shortName": "dttbFormRadio",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormRadio",
      "keywords": "api attribute binding column config configuration data-tablar datatablar directive dttbformradio element form formtypeconfig label model object optional radio required table valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormSelectmultiple",
      "shortName": "dttbFormSelectmultiple",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormSelectmultiple",
      "keywords": "api attribute binding column config configuration data-tablar datatablar directive disable disabled dttbformselectmultiple element enable form formtypeconfig label model object optional required select-multiple table valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormSelectsingle",
      "shortName": "dttbFormSelectsingle",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormSelectsingle",
      "keywords": "api attribute binding column config configuration data-tablar datatablar directive disable disabled dttbformselectsingle element enable form formtypeconfig label model object optional required select-single table valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormTextarea",
      "shortName": "dttbFormTextarea",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormTextarea",
      "keywords": "api attribute binding column config configuration data-tablar datatablar directive disable disabled dttbformtextarea element enable form formtypeconfig label model object optional required table textarea valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbFormTypeahead",
      "shortName": "dttbFormTypeahead",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "dttbFormTypeahead",
      "keywords": "api attribute binding column config configuration data-tablar datatablar directive disable disabled dttbformtypeahead element enable form formtypeconfig label model object optional required table typeahead valid validation validations"
    },
    {
      "section": "api",
      "id": "dataTablar.directive:dttbTable",
      "shortName": "dttbTable",
      "type": "directive",
      "moduleName": "dataTablar",
      "shortDescription": "Main directive of data-tablar module. It is used to generate data-tablar table with the configuration generated from builder.",
      "keywords": "api binding builder config configdatatablar configuration data data-tablar datatablar directive dttb-table generate generated main module object required table"
    },
    {
      "section": "api",
      "id": "dataTablar.filter:dttbCellFormat",
      "shortName": "dttbCellFormat",
      "type": "filter",
      "moduleName": "dataTablar",
      "shortDescription": "dttbCellFormat",
      "keywords": "$filter api array boolean customize datatablar diplays displayas displayasconfig displayasconfiguration dttb_date_display_format dttbcellformat filter input moment number object table text type"
    },
    {
      "section": "api",
      "id": "dataTablar.filter:dttbRowSummary",
      "shortName": "dttbRowSummary",
      "type": "filter",
      "moduleName": "dataTablar",
      "shortDescription": "dttbRowSummary",
      "keywords": "$filter api array average column columnkey data datatablar dttbhelperservice dttbrowsummary filter max min objects rows sum summarize summarized summary table type values"
    },
    {
      "section": "api",
      "id": "dataTablar.service:dttbExporterService",
      "shortName": "dttbExporterService",
      "type": "service",
      "moduleName": "dataTablar",
      "shortDescription": "dttbExporterService",
      "keywords": "$filter api automatically body check checked checkvalidfilename codes columns configuration csv csvbody csvfooter csvheader data data-tablar datatablar download downloadcsvfile dttb_export_delimiter dttbexporterservice export exportconfig exportcsv file filename footers format generate generatecsvbody generatecsvheader generated generateprinttable headers html includes invalid method object options print-able printed printing printingconfig rows scope service table triggered user valid"
    },
    {
      "section": "api",
      "id": "dataTablar.service:dttbHelperService",
      "shortName": "dttbHelperService",
      "type": "service",
      "moduleName": "dataTablar",
      "shortDescription": "dttbHelperService",
      "keywords": "$window accidentally action api array arrayofnestedobjects bind bindbeforeunload calculated cancels check checkvalidfilename compare compared condition create current currentkey currentnumrows currentpage datatablar delete displayed dttbhelperservice elements error errormsg event example false file filename finishes flatted flattenarrayofnestedobjects flattenrecursive function functions generate generatepagination generatesolvingerrorguide guide key leaving message method number numpagesdisplaying numrowsperpage numtotalrows object objects order pagination prevent process property provided recursive reference root rows service solve suberrormsg sum summed support table target total true udpate unbind unbindbeforeunload update url user valid wether window"
    },
    {
      "section": "api",
      "id": "dataTablar.service:dttbRequestService",
      "shortName": "dttbRequestService",
      "type": "service",
      "moduleName": "dataTablar",
      "shortDescription": "dttbRequestService",
      "keywords": "$http accidentally action api bind called callrequest codes communicating config configuration conversionrequest conversionresponse create data datatablar delete dttbrequestservice event formatted function generated handleoptionajaxrequest handlerouteparams headers html http httpconfig includes inithttpobject initialize leaving method notification object order params prevent provided reference remote request requestconfig requesting requests resource row server service services table udpate url window"
    },
    {
      "section": "api",
      "id": "dataTablar.service:dttbValidatorService",
      "shortName": "dttbValidatorService",
      "type": "service",
      "moduleName": "dataTablar",
      "shortDescription": "dttbValidatorService",
      "keywords": "api array based boolean check checkcontaining checkdatesince checkdateuntil checked checkequalto checkgreaterequal checkinarray checklengthmax checklengthmin checklessequal checklessthan checknot checkoptionsmax checkoptionsmin checkrequired checkrequiredstrictly checktype checkwithserver compare condition datatablar dttbvalidatorservice email form generateprinttable getvartype inequality length maximum method milestone minimum moment number obj passed provided reference regex required returns service services special specific strictly string tag text true type types url valid validate validation validations"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};