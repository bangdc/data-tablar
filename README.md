# data-tablar

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Demo

* [Users management](http://datatablar.com/#/)
* [Authentication and permissions](http://demo-auth.datatablar.com/#/)
* [Proxy - Wrapper your APIs](http://demo-proxy.datatablar.com/#/)
* [Film database](http://demo-film.datatablar.com/#/)

## Builder

Builder makes it easy to generate a configuration JSON object for datatablar table.
Check out [here](http://builder.datatablar.com/#/).


## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Userguide

Check out [here](http://userguide.datatablar.com/).